-- Techwert Inventory Manager DB Source.

CREATE TABLE tw_warehouse (
	id INTEGER NOT NULL CONSTRAINT warehouse_PK PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL DEFAULT '',
	size TEXT NOT NULL DEFAULT '',
	code TEXT NOT NULL DEFAULT ''
);

CREATE TABLE tw_ware_type (
	id INTEGER NOT NULL CONSTRAINT ware_type_PK PRIMARY KEY AUTOINCREMENT,
	type TEXT NOT NULL UNIQUE
);

CREATE TABLE tw_rack (
	id INTEGER NOT NULL CONSTRAINT rack_PK PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL DEFAULT '',
	position TEXT NOT NULL DEFAULT '',
	row TEXT NOT NULL DEFAULT '',
	column TEXT NOT NULL DEFAULT '',
	depth TEXT NOT NULL DEFAULT '',
	warehouse_id INTEGER NOT NULL,
	CONSTRAINT rack_warehouse_FK
		FOREIGN KEY (warehouse_id) REFERENCES tw_warehouse (id)
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE tw_container (
	id INTEGER NOT NULL CONSTRAINT container_PK PRIMARY KEY AUTOINCREMENT,
	identifier TEXT NOT NULL UNIQUE,
	dimension TEXT NOT NULL DEFAULT '',
	rack_id INTEGER NOT NULL,
	CONSTRAINT container_rack_FK
		FOREIGN KEY (rack_id) REFERENCES tw_rack (id)
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE tw_item (
	id INTEGER NOT NULL CONSTRAINT item_PK PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL UNIQUE,
	quantity INTEGER NOT NULL DEFAULT 0,
	container_id NOT NULL,
	CONSTRAINT item_container_FK
		FOREIGN KEY (container_id) REFERENCES tw_container (id)
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE tw_property (
	id INTEGER NOT NULL CONSTRAINT property_PK PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL UNIQUE,
	value TEXT NOT NULL DEFAULT '',
	data_type TEXT NOT NULL DEFAULT 'string'
);

CREATE TABLE tw_user (
	id INTEGER NOT NULL CONSTRAINT user_PK PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL,
	last_name TEXT NOT NULL,
	rut TEXT NOT NULL DEFAULT '',
	nickname TEXT NOT NULL UNIQUE,
	password BLOB NOT NULL,
	salt BLOB NOT NULL,
	CONSTRAINT user_name_UQ UNIQUE (name, last_name)
);

CREATE TABLE tw_session (
	id INTEGER NOT NULL CONSTRAINT session_PK PRIMARY KEY AUTOINCREMENT,
	event_time TEXT NOT NULL DEFAULT '',
	action_name TEXT NOT NULL DEFAULT 'login',
	user_id INTEGER NOT NULL,
	CONSTRAINT session_user_FK FOREIGN KEY (user_id) REFERENCES tw_user (id) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE tw_capability (
	id INTEGER NOT NULL CONSTRAINT capability_PK PRIMARY KEY AUTOINCREMENT,
	capability_name TEXT NOT NULL UNIQUE,
	description TEXT NOT NULL DEFAULT ''
);

CREATE TABLE tw_preference (
	id INTEGER NOT NULL CONSTRAINT preference_PK PRIMARY KEY AUTOINCREMENT,
	preference_name TEXT NOT NULL UNIQUE,
	preference_value TEXT NOT NULL DEFAULT '',
	data_type TEXT NOT NULL DEFAULT 'STRING'
);

CREATE TABLE tw_provider (
	id INTEGER NOT NULL CONSTRAINT provider_PK PRIMARY KEY AUTOINCREMENT,
	code TEXT NOT NULL,
	provider_name NOT NULL,
	CONSTRAINT provider_code_UQ UNIQUE code,
	CONSTRAINT provider_name_UQ UNIQUE provider_name
);

-- N:M RELATIONSHIPS:
CREATE TABLE tw_user_container (
	user_id INTEGER NOT NULL,
	container_id INTEGER NOT NULL,
	action_date TEXT NOT NULL,
	action_name TEXT NOT NULL DEFAULT 'open',
	CONSTRAINT user_container_PK PRIMARY KEY (user_id, container_id),
	CONSTRAINT user_container_container_FK FOREIGN KEY (container_id) REFERENCES tw_container (id) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT user_container_user_FK FOREIGN KEY (user_id) REFERENCES tw_user (id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE tw_user_capabilities (
	user_id INTEGER NOT NULL,
	capability_id INTEGER NOT NULL,
	permission TEXT NOT NULL DEFAULT 'READ',
	CONSTRAINT user_capabilities_PK PRIMARY KEY (user_id, capability_id),
	CONSTRAINT user_caps_user_FK FOREIGN KEY (user_id) REFERENCES tw_user (id) ON UPDATE CASCADE ON DELETE RESTRICT,
	CONSTRAINT user_caps_capabilty_FK FOREIGN KEY (capability_id) REFERENCES tw_capability (id) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE tw_item_properties (
	item_id INTEGER NOT NULL,
	property_id INTEGER NOT NULL,
	CONSTRAINT item_properties_PK PRIMARY KEY (item_id, property_id),
	CONSTRAINT item_props_property_FK FOREIGN KEY (property_id) REFERENCES tw_property (id) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT item_props_item_FK FOREIGN KEY (item_id) REFERENCES tw_item (id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE tw_warehouse_store_type (
	warehouse_id INTEGER NOT NULL,
	ware_type_id INTEGER NOT NULL,
	CONSTRAINT warehouse_store_type_PK
		PRIMARY KEY(warehouse_id, ware_type_id),
	CONSTRAINT wareh_sttype_sttype_FK
		FOREIGN KEY (ware_type_id) REFERENCES tw_ware_type (id)
		ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT wareh_sttype_wareh_FK
		FOREIGN KEY (warehouse_id) REFERENCES tw_warehouse (id)
		ON UPDATE CASCADE ON DELETE CASCADE
);

ALTER TABLE tw_inventory ADD COLUMN provider_id INTEGER NOT NULL CONSTRAINT item_provider_FK REFERENCES tw_provider (id);